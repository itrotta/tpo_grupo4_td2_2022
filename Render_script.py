import open3d as o3d
import pandas as pd
import numpy as np


dist_centro=270 #Medida con el scanner

df=pd.read_csv(r'C:\Users\Ignacio\Desktop\scan_fin.csv', sep=';')  #Leo los datos del csv

df.set_index(['thetha','altura'],drop=False,inplace=True) #Nombre de las columnas

df = df.rename(columns={'thetha':'angulo','altura':'z'})

#df = df.groupby(['altura','thetha']).mean() #Promedio de las muestras para un mismo angulo y altura

#df['radio'] = -0.0425 * df.voltage + 341.87 #Se aplica la curva que convierte tension a distancia
#df['radio'] = -0.0341 * df.voltage + 268.73 #Se aplica la curva que convierte tension a distancia
df['radio'] =-0.02*(df.voltage)+301.97 #Se aplica la curva que convierte tension a distancia
df.radio=dist_centro - df.radio #Se calcula la distancia del centro de la pieza al punto de medicion
df=df[df.radio>0] #Se eliminan las muestras mayores a la distancia al centro 

df.angulo = df.angulo * (16*2*np.pi/2048)

#df=df.rolling(10,on='angulo',min_periods=1)
#df=df.mean()
df['x']=df.radio * np.cos(df.angulo)
df['y']=df.radio * np.sin(df.angulo)

point_cloud = np.zeros((df.x.size,3))
point_cloud[:,0]= df.x 
point_cloud[:,1]= df.y
point_cloud[:,2]= df.z*2

#Representacion nube de puntos
pcd = o3d.geometry.PointCloud()
pcd.points = o3d.utility.Vector3dVector(point_cloud)
pcd.colors = o3d.utility.Vector3dVector(point_cloud/255)
pcd.normals = o3d.utility.Vector3dVector(point_cloud)
o3d.visualization.draw_geometries([pcd])

# #Metodo ballpivoting
# distances = pcd.compute_nearest_neighbor_distance()
# avg_dist = np.mean(distances)
# radius = 6 * avg_dist
# pcd,ind=pcd.remove_radius_outlier(nb_points=15, radius=1.5*radius) 

# pcd.estimate_normals()
# #o3d.io.write_point_cloud('C:Users/adrie/Documents/test/scan1_mod.csv',pcd,write_ascii=False)

# rec_mesh =  o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(pcd,o3d.utility.DoubleVector([radius,radius**2]))
# rec_mesh = rec_mesh.simplify_quadric_decimation(1000)
# rec_mesh.remove_degenerate_triangles()
# rec_mesh.remove_duplicated_triangles()
# rec_mesh.remove_duplicated_vertices()
# rec_mesh.remove_non_manifold_edges()
# rec_mesh.paint_uniform_color([0, 0.7, 1])

# o3d.io.write_triangle_mesh("C:/Users/adrie/Documents/test/3dObject/lpc845_piramide_ballpivoting_sprom_complete.stl", rec_mesh)


#Metodo Poisson
poisson_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(pcd, depth=4, width=0, scale=1.1, linear_fit=False)[0]
bbox = pcd.get_axis_aligned_bounding_box()
p_mesh_crop = poisson_mesh.crop(bbox)
o3d.io.write_triangle_mesh(r"C:\Users\Ignacio\Desktop\rectangulo.ply", p_mesh_crop)





# df=df.rolling(10,on='angulo',min_periods=1)
# df=df.mean()
# df['x']=df.radio * np.cos(df.angulo)
# df['y']=df.radio * np.sin(df.angulo)

# point_cloud = np.zeros((df.x.size,3))
# point_cloud[:,0]= df.x 
# point_cloud[:,1]= df.y
# point_cloud[:,2]= df.z*1

# #Representacion nube de puntos
# pcd = o3d.geometry.PointCloud()
# pcd.points = o3d.utility.Vector3dVector(point_cloud)
# pcd.colors = o3d.utility.Vector3dVector(point_cloud/255)
# pcd.normals = o3d.utility.Vector3dVector(point_cloud)
# o3d.visualization.draw_geometries([pcd])

# #Metodo ballpivoting
# distances = pcd.compute_nearest_neighbor_distance()
# avg_dist = np.mean(distances)
# radius = 6 * avg_dist
# pcd,ind=pcd.remove_radius_outlier(nb_points=15, radius=1.5*radius) 

# pcd.estimate_normals()
# #o3d.io.write_point_cloud('C:Users/adrie/Documents/test/scan1_mod.csv',pcd,write_ascii=False)

# rec_mesh =  o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(pcd,o3d.utility.DoubleVector([radius,radius**2]))
# rec_mesh = rec_mesh.simplify_quadric_decimation(1000)
# rec_mesh.remove_degenerate_triangles()
# rec_mesh.remove_duplicated_triangles()
# rec_mesh.remove_duplicated_vertices()
# rec_mesh.remove_non_manifold_edges()
# rec_mesh.paint_uniform_color([0, 0.7, 1])

# o3d.io.write_triangle_mesh('C:/Users/adrie/Documents/test/3dObject/lpc845_piramide_ballpivoting_cprom_complete.stl', rec_mesh)


# #Metodo Poisson
# poisson_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(pcd, depth=4, width=0, scale=1.1, linear_fit=False)[0]
# bbox = pcd.get_axis_aligned_bounding_box()
# p_mesh_crop = poisson_mesh.crop(bbox)
# o3d.io.write_triangle_mesh('C:/Users/adrie/Documents/test/3dObject/lpc845_piramide_poisson_cprom_complete.ply', p_mesh_crop)