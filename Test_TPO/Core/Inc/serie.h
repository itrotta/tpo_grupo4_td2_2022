/*
 * serie.h
 *
 *  Created on: Dec 12, 2022
 *      Author: adrie
 */

#include "main.h"
#include "tareas.h"
void enviarSerie(uint8_t );
void serieFreeRTOS_puts(uint16_t *, uint32_t );
void serieFreeRTOS_gets(uint8_t *, uint32_t );
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *);
uint8_t Mde_Serial(int16_t);
