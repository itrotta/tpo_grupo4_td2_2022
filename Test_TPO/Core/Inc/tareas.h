/*
 * tareas.h
 *
 *  Created on: Dec 12, 2022
 *      Author: adrie
 */

#ifndef INC_TAREAS_H_
#define INC_TAREAS_H_

#include "main.h"
#include "ADC.h"

#define TOTAL_H 5
#define MAX_LECTURAS 200
#define ANGLE_STEP 16 // PAso de angulo de 3°
#define HEIGHT_STEP 256 //Paso de altura de 2 mm

#define WAIT 	0
#define CHAR_INIT   254
#define CHAR_FIN    253
#define CHAR_STOP   252
#define CHAR_SCAN   249
#define CHAR_CAL    248
#define CHAR_MAN    247

extern TaskHandle_t handler_scan;
extern TaskHandle_t handler_serieSND;
extern TaskHandle_t handler_serieRCV;
extern TaskHandle_t handler_Cal;
extern TaskHandle_t handler_Man;

extern QueueHandle_t cola_tx;
extern QueueHandle_t cola_manual;

extern xSemaphoreHandle semaphore_scan;
extern xSemaphoreHandle semaforo_tx;
extern xSemaphoreHandle semaphore_cal;
extern xSemaphoreHandle semaphore_manual;

extern uint8_t flag_reset;
extern uint8_t flag_medicion;

void initTareas(void);
void Senial (uint8_t );


#endif /* INC_TAREAS_H_ */
