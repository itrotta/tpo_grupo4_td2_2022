/*
 * ADC.h
 *
 *  Created on: Dec 8, 2022
 *      Author: adrie
 */

#ifndef INC_ADC_H_
#define INC_ADC_H_
#include "main.h"

#define TICKS_ADC_MS		(1)
#define DELAY_ADC			(64)
#define demora_software(X)	{for(int i=0; i<(X); i++){}}


extern uint32_t ticks_adc;


void promedio (uint16_t );
uint16_t lecturaADC(void);

#endif /* INC_ADC_H_ */
