/*
 * motor.h
 *
 *  Created on: Oct 9, 2022
 *      Author: adrie
 */

#ifndef INC_MOTOR_H_
#define INC_MOTOR_H_

#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "freeRTOS.h"
#include "task.h"

extern int8_t flag_stepper;



void Motor1_UP(int32_t);
void Motor1_DOWN(int32_t);

void Motor2_Clockwise(int32_t);
void Motor2_CounterClockwise(int32_t );

//void MotorControl(void);
/*	}if (flag_stepper==2){
		if(pasos_M2>0){
			M2_ClockWise_FullStep();
			TimerStart(M_TIMER,PPS,M_Timer_Handler,MILI);
		}if(pasos_M2<0){
			M2_Counter_ClockWise_FullStep();
			TimerStart(M_TIMER,PPS,M_Timer_Handler,MILI);
		}
	}*/

//---------------------------------------------------------------------------------------------------------
//#########################################################################################################
//---------------------------------------------------------------------------------------------------------


//DRIVERS
//---------------------------------------------------------------------------------------------------------
//#########################################################################################################
//---------------------------------------------------------------------------------------------------------
extern int32_t pasos_M1, pasos_M2;

void M1_ClockWise_FullStep(void);
void M1_CounterClockWise_FullStep(void);
void M2_ClockWise_FullStep(void);

#endif /* INC_MOTOR_H_ */
