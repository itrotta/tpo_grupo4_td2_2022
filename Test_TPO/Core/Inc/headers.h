/*
 * headers.h
 *
 *  Created on: Dec 9, 2022
 *      Author: adrie
 */

#ifndef INC_HEADERS_H_
#define INC_HEADERS_H_

#include "ADC.h"
#include "motor.h"
#include "queue.h"
#include "semphr.h"
#include "serie.h"
#include "tareas.h"

#endif /* INC_HEADERS_H_ */
