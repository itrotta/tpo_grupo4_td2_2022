/*
 * ADC.c
 *
 *  Created on: Dec 8, 2022
 *      Author: adrie
 */
#include "ADC.h"

uint32_t ticks_adc=0;



uint16_t lecturaADC(){
	//FUNCION PARA LEER EL ADC
	uint16_t dato_ch0=0;
	if((HAL_GetTick()-ticks_adc)>= TICKS_ADC_MS) //TIEMPO DE POLLING
	  {
			ticks_adc = HAL_GetTick();
			HAL_ADC_Start(&hadc1);
			HAL_ADC_PollForConversion(&hadc1, 10);
			demora_software(DELAY_ADC); //AGREGAR DEMORA NO BLOQUEANTE - DELAY_ADC ES EL TIEMPO DE CONVERSION?
			dato_ch0 = HAL_ADC_GetValue(&hadc1);
			HAL_ADC_Stop(&hadc1);

	  }else
	  	  return -1;
	return dato_ch0;
}
void promedio (uint16_t dato){
		static uint32_t suma=0;
		uint32_t aux=0;
		suma+=dato;
		//xQueueSend(cola_rx,&dato,portMAX_DELAY);
		if(flag_medicion){
			suma/=200;//Promedio de 128 muestras
			aux=suma/1000+48;
			xQueueSend(cola_tx,&aux,portMAX_DELAY);
			suma%=1000;
			aux=suma/100+48;
			xQueueSend(cola_tx,&aux,portMAX_DELAY);
			suma%=100;
			aux=suma/10+48;
			xQueueSend(cola_tx,&aux,portMAX_DELAY);
			suma%=10;
			aux=suma+48;
			xQueueSend(cola_tx,&aux,portMAX_DELAY);
			aux=10;
			xQueueSend(cola_tx,&aux,portMAX_DELAY);
			suma=0;
			flag_medicion=0;
		}
}
