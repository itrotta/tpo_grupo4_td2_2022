/*
 * tareas.c
 *
 *  Created on: Dec 12, 2022
 *      Author: adrie
 */


#include "tareas.h"
#include "serie.h"

TaskHandle_t handler_scan;
TaskHandle_t handler_serieSND;
TaskHandle_t handler_serieRCV;
TaskHandle_t handler_Cal;
TaskHandle_t handler_Man;

QueueHandle_t cola_tx;
QueueHandle_t cola_manual;

xSemaphoreHandle semaphore_scan;
xSemaphoreHandle semaforo_tx;
xSemaphoreHandle semaphore_cal;
xSemaphoreHandle semaphore_manual;

uint8_t flag_reset=0;
uint8_t flag_medicion=0;

void tareaScan (void *p){
	int16_t i=0,n=0,k=0;
	int16_t dato=0;
	xSemaphoreTake(semaphore_scan,portMAX_DELAY);
	for(;;){
		for(n=0;n<30 && flag_reset==0;n++){	// TOTAL_H
			for(k=0;k<2048/ANGLE_STEP && flag_reset==0;k++){
				for(i=0;i<MAX_LECTURAS && flag_reset==0;i++){
							vTaskDelay(2/portTICK_RATE_MS);
							if(i==(MAX_LECTURAS-1))
								flag_medicion=1;
							if((dato=lecturaADC())!=-1)
								promedio(dato);
				}Motor2_Clockwise(ANGLE_STEP);
			}Motor1_UP(HEIGHT_STEP);
		}if (flag_reset==0) enviarSerie(CHAR_STOP);
	xSemaphoreTake(semaphore_scan,portMAX_DELAY);
	}
}

void tareaCal(void *p){
	int16_t dato=0;
	xSemaphoreTake(semaphore_cal,portMAX_DELAY);
	for(;;){
		for(uint8_t n=0;n<50 && flag_reset==0;n++){ //A un paso de 2mm, 50 repeticiones son 100mm
			for(uint16_t i=0;i<MAX_LECTURAS && flag_reset==0;i++){
				vTaskDelay(2/portTICK_RATE_MS);
				if(i==(MAX_LECTURAS-1))
					flag_medicion=1;
				if((dato=lecturaADC())!=-1) //ESTO ESTA MAL PORQUE SI NO ENTRA LO CUENTA COMO MEDICION
					promedio(dato);
			}Motor1_UP(512); //Avance de 2mm
		}if (flag_reset==0) enviarSerie(CHAR_STOP);
		xSemaphoreTake(semaphore_cal,portMAX_DELAY);
	}
}

void tareaManual(void *p){
	uint8_t distance;
	for(;;){
		xQueueReceive(cola_manual,&distance,portMAX_DELAY);
		if(distance<100)
			Motor1_UP(distance*256);//Si distance = 1mm => el motor se movera 256 paso0s
		else if(distance>128)
			Motor1_DOWN((distance-128)*256);
		if(flag_reset==0) enviarSerie(CHAR_STOP);
	}
}



void tareaSerie_snd (void *p){
	uint16_t dato;
	for(;;){
		xQueueReceive(cola_tx,&dato,portMAX_DELAY);
		serieFreeRTOS_puts(&dato,1);
	}
}

void tareaSerie_rcv (void *p){
	uint8_t dato=0;
	uint8_t sig=0;
	for(;;){
		serieFreeRTOS_gets(&dato,1);
		if((sig=Mde_Serial(dato))!=0)
			Senial(sig);
	}
}




void Senial (uint8_t sig){
	static uint8_t estado=0;
	static uint8_t flag_manual=0;
	if (sig>=CHAR_MAN)
		estado=sig;
	switch (estado){
		case WAIT://No hay señal
			break;
		case CHAR_SCAN:
			for(int i=0;i<3;i++){
				HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
				vTaskDelay(200/portTICK_RATE_MS);
				HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
				vTaskDelay(200/portTICK_RATE_MS);
			}
			flag_reset=0;
			estado=WAIT;

			xSemaphoreGive(semaphore_scan);
			break;
		case CHAR_STOP://Stop
			flag_reset=1;
			vTaskDelay(200/portTICK_RATE_MS); //Le doy tiempo a las otras tareas que se reinicien
			for(int i=0;i<10;i++){
							HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
							vTaskDelay(60/portTICK_RATE_MS);
							HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
							vTaskDelay(60/portTICK_RATE_MS);
			}
			estado=WAIT;
			break;

		case CHAR_CAL:
			for(int i=0;i<5;i++){
					HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
					vTaskDelay(200/portTICK_RATE_MS);
					HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
					vTaskDelay(200/portTICK_RATE_MS);
				}
			flag_reset=0;
			estado=WAIT;
			xSemaphoreGive(semaphore_cal);
			break;

		case CHAR_MAN:
			if (flag_manual==1){
				if (sig>0 && sig<229){//ver como implementa los negativos
					flag_reset=0;
					xQueueSend(cola_manual,&sig,portMAX_DELAY);
				}
				estado=WAIT;
				flag_manual=0;
			}else flag_manual=1;
			break;
	}
}

void initTareas(void){

	  xTaskCreate(	tareaScan,
	  		  	  	"scan",
	  				configMINIMAL_STACK_SIZE,
	  				NULL,
	  				tskIDLE_PRIORITY+1,
					&handler_scan);
	 xTaskCreate(	tareaSerie_snd,
	   		  	  	"Serie_SND",
	   				configMINIMAL_STACK_SIZE,
	   				NULL,
	   				tskIDLE_PRIORITY+2,
	 				&handler_serieSND);
	 xTaskCreate(	tareaSerie_rcv,
	   		  	  	"Serie_RCV",
	   				configMINIMAL_STACK_SIZE,
	   				NULL,
	   				tskIDLE_PRIORITY+3,
	 				&handler_serieRCV);
	 xTaskCreate(	tareaCal,
	   		  	  	"calib",
	   				configMINIMAL_STACK_SIZE,
	   				NULL,
	   				tskIDLE_PRIORITY+1,
	 				&handler_Cal);
	 xTaskCreate(	tareaManual,
	   		  	  	"manual",
	   				configMINIMAL_STACK_SIZE,
	   				NULL,
	   				tskIDLE_PRIORITY+1,
	 				&handler_Man);

	 cola_tx=xQueueCreate(1000,sizeof(uint8_t));
	 cola_manual=xQueueCreate(100,sizeof(uint8_t));

	 semaphore_scan = xSemaphoreCreateBinary();
	 semaphore_cal = xSemaphoreCreateBinary();
	 semaforo_tx = xSemaphoreCreateBinary();
	 semaphore_manual = xSemaphoreCreateBinary();
}
