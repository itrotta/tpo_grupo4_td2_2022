/*
 * serie.c
 *
 *  Created on: Dec 12, 2022
 *      Author: adrie
 */

#include "serie.h"
#define INICIO 1
#define WAIT_SIGNAL 2
#define WAIT_CHAR_FIN 3

uint8_t Mde_Serial(int16_t dato){
	static uint8_t state=INICIO;
	static uint8_t aux=0;
	uint8_t r=0;

	switch (state){
	case INICIO:
		if(dato==CHAR_INIT)
			state=WAIT_SIGNAL;
		break;
	case WAIT_SIGNAL:
		if (dato<CHAR_FIN){
			state=WAIT_CHAR_FIN;
			aux=dato;
		}else
			state=INICIO;
		break;
	case WAIT_CHAR_FIN:
		if (dato==CHAR_FIN)
			r=aux;
	    state=INICIO;
		break;
	default :
		state=INICIO;
		aux=0;
		r=0;
	}return r;
}

void enviarSerie(uint8_t dato){
	uint8_t aux=254;
	if(dato==CHAR_STOP){
		xQueueSend(cola_tx,&aux,portMAX_DELAY);
		aux=253;
		xQueueSend(cola_tx,&dato,portMAX_DELAY);
		xQueueSend(cola_tx,&aux,portMAX_DELAY);
	}
	else xQueueSend(cola_tx,&dato,portMAX_DELAY);
}

void serieFreeRTOS_puts(uint16_t *datos, uint32_t len)
{
	HAL_UART_Transmit(&huart2, (uint8_t*)datos, 1, 0xFFFF);
}
void serieFreeRTOS_gets(uint8_t *datos, uint32_t len)
{
	HAL_UART_Receive_IT(&huart2, (uint8_t*)datos, len);
	xSemaphoreTake(semaforo_tx,portMAX_DELAY);
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	BaseType_t p;
	UNUSED(huart);
	xSemaphoreGiveFromISR(semaforo_tx,&p);
	portEND_SWITCHING_ISR(p);
}
