/*
 * Motor.c
 *
 *  Created on: Oct 4, 2022
 *      Author: adrie
 */

/* Funciones para activar las bobinas del motor

 PARAMETER :
 	 GPIO_PIN_RESET = LOW
 	 GPIO_PIN_SET = HIGH

HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,PARAMETER)
HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,)
HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,)
HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,)

HAL_GPIO_WritePin (GPIOB,M2_CoilA_Pin,)
HAL_GPIO_WritePin (GPIOB,M2_CoilB_Pin,)
HAL_GPIO_WritePin (GPIOB,M2_CoilC_Pin,)
HAL_GPIO_WritePin (GPIOB,M2_CoilD_Pin,)


*/

//PRIMITIVAS
//---------------------------------------------------------------------------------------------------------
//#########################################################################################################
//---------------------------------------------------------------------------------------------------------

#include "motor.h"
#include "main.h"

#define PPS 10 // ms que enciende la bobina

/* Paso de varilla = 8mm
 * Pasos Motor = 2048
*/
int8_t flag_stepper=0;

void Motor1_UP(int32_t steps){
	pasos_M1=steps;
	flag_stepper=1;
	while(flag_stepper==1 && flag_reset==0){
			if(pasos_M1>=0){
				M1_ClockWise_FullStep();
			}
		vTaskDelay(5/portTICK_RATE_MS);
	}
}

void Motor1_DOWN(int32_t steps){
	pasos_M1=steps;
	flag_stepper=1;
	while(flag_stepper==1 && flag_reset==0){
			if(pasos_M1>=0){
				//M1_ClockWise_FullStep();
				M1_CounterClockWise_FullStep();
			}
		vTaskDelay(5/portTICK_RATE_MS);
	}
}


void Motor2_Clockwise(int32_t steps ){
	pasos_M2=steps;
	flag_stepper=1;
	while(flag_stepper==1 && flag_reset==0){
			if(pasos_M2>=0){
				M2_ClockWise_FullStep();
			}
		vTaskDelay(6/portTICK_RATE_MS);
	}
}
/*void Motor2_CounterClockwise(uint32_t x){
	while(flag_stepper==1){
		if(pasos_M1>=0){
			M1_ClockWise_FullStep();
		}
		vTaskDelay(1000/portTICK_RATE_MS);
	}
}*/

/*void MotorControl(void){
	if (flag_stepper==1){
		if(pasos_M1>0){
			M1_ClockWise_FullStep();
			TimerStart(M_TIMER,PPS,M_Timer_Handler,MILI);
		}if(pasos_M1<0){
			M1_CounterClockWise_FullStep();
			TimerStart(M_TIMER,PPS,M_Timer_Handler,MILI);
		}
	}if (flag_stepper==2){
		if(pasos_M2>0){
			M2_ClockWise_FullStep();
			TimerStart(M_TIMER,PPS,M_Timer_Handler,MILI);
		}if(pasos_M2<0){
			M2_Counter_ClockWise_FullStep();
			TimerStart(M_TIMER,PPS,M_Timer_Handler,MILI);
		}
	}*/
//}

//---------------------------------------------------------------------------------------------------------
//#########################################################################################################
//---------------------------------------------------------------------------------------------------------


//DRIVERS
//---------------------------------------------------------------------------------------------------------
//#########################################################################################################
//---------------------------------------------------------------------------------------------------------
#define A 0
#define B 1
#define C 2
#define D 3
#define FIN 4
int32_t pasos_M1=0, pasos_M2=0;

void M1_ClockWise_FullStep(void){

	static uint8_t estado=0,next_estado=0;
	switch (estado){
	case A:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=B;
		pasos_M1--;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=B;
		}
		break;

	case B:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=C;
		pasos_M1--;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=C;
		}
		break;

	case C:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=D;
		pasos_M1--;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=D;
		}
		break;
	case D:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_SET);
		pasos_M1--;
		estado=A;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=A;
		}

		break;

	case FIN:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		flag_stepper=-1;
		estado=next_estado;
		break;
	}
}

void M1_CounterClockWise_FullStep(void){

	static uint8_t estado=0,next_estado=0;
	switch (estado){
	case A:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=D;
		pasos_M1++;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=D;
		}
		break;

	case B:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=A;
		pasos_M1++;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=A;
		}
		break;

	case C:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=B;
		pasos_M1++;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=B;
		}
		break;
	case D:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_SET);
		pasos_M1++;
		estado=C;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=C;
		}

		break;

	case FIN:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		flag_stepper=-1;
		estado=next_estado;
		break;
	}
}


void M2_ClockWise_FullStep(void){

	static uint8_t estado=0,next_estado=0;
	switch (estado){
	case A:
		HAL_GPIO_WritePin (GPIOB,M2_CoilA_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilD_Pin,GPIO_PIN_RESET);
		estado=B;
		pasos_M2--;
		if ((pasos_M2)==0){
			estado=FIN;
			next_estado=B;
		}
		break;

	case B:
		HAL_GPIO_WritePin (GPIOB,M2_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilB_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilD_Pin,GPIO_PIN_RESET);
		estado=C;
		pasos_M2--;
		if ((pasos_M2)==0){
			estado=FIN;
			next_estado=C;
		}
		break;

	case C:
		HAL_GPIO_WritePin (GPIOB,M2_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilC_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilD_Pin,GPIO_PIN_RESET);
		estado=D;
		pasos_M2--;
		if ((pasos_M2)==0){
			estado=FIN;
			next_estado=D;
		}
		break;
	case D:
		HAL_GPIO_WritePin (GPIOB,M2_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilD_Pin,GPIO_PIN_SET);
		pasos_M2--;
		estado=A;
		if ((pasos_M2)==0){
			estado=FIN;
			next_estado=A;
		}

		break;

	case FIN:
		HAL_GPIO_WritePin (GPIOB,M2_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M2_CoilD_Pin,GPIO_PIN_RESET);
		flag_stepper=0;
		estado=next_estado;
		break;
	}
}


/*void M1_CounterClockWise_FullStep(void){

	static uint8_t estado=0,next_estado=0;
	switch (estado){
	case A:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=D;
		pasos_M1++;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=D;
		}
		break;

	case B:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=A;
		pasos_M1++;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=A;
		}
		break;

	case C:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		estado=B;
		pasos_M1++;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=B;
		}
		break;
	case D:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_SET);
		pasos_M1++;
		estado=C;
		if ((pasos_M1)==0){
			estado=FIN;
			next_estado=C;
		}

		break;

	case FIN:
		HAL_GPIO_WritePin (GPIOA,M1_CoilA_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOA,M1_CoilB_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilC_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin (GPIOB,M1_CoilD_Pin,GPIO_PIN_RESET);
		flag_stepper=-1;
		estado=next_estado;
		break;
	}
}
*/

//---------------------------------------------------------------------------------------------------------
//#########################################################################################################
//---------------------------------------------------------------------------------------------------------
